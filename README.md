![Build Status](https://gitlab.com/vilanify/vuepress/badges/master/pipeline.svg?style=flat-square)
---

Example VuePress project website using GitLab Pages.

Learn more about GitLab Pages at https://about.gitlab.com/product/pages and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.

---

## GitLab CI

```
image: node:9.11.1

pages:
  cache:
    paths:
    - node_modules/

  script:
  - yarn install
  - yarn build

  artifacts:
    paths:
    - public
  
  only:
  - master

```

This sets up a `node9.11.1` environment, then uses `yarn install` to install dependencies and `yarn build` to build out the website to the `./public` directory.
It also caches the `node_modules` directory to speed up subsequent builds.

## Building locally

This project uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can get started.

```
npm install -g yarn
```

Then you need to install the project dependencies:

```
yarn install
```

Now you're ready to go.
To run the local dev server just use the following command:

```
yarn start
```

Your website should be available at [http://localhost:8080/vuepress]


## Project name
You'll need to set the correct base in docs/.vuepress/config.js.

If you are deploying to https://<USERNAME or GROUP>.gitlab.io/, you can omit base as it defaults to "/".

If you are deploying to https://<USERNAME or GROUP>.gitlab.io/<REPO>/, (i.e. your repository is at https://gitlab.com/<USERNAME>/<REPO>), set base to "/<REPO>/".
